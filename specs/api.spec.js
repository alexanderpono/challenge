// import { GetChallenges } from '../framework/services/GetChallenges.service';

// describe('Это описание теста', () => {
//     test('sum', async ()=>{
//         const r = await new GetChallenges.get();
//         expect(r.status).toBe(200);
//     });
// })


import { GetChallenges } from '../framework/services/GetChallenges.service';

describe ('Это описание теста', () =>{
    test('Получить список заданий', async () => {
        const {status} = await new GetChallenges().get();
        expect(status).toBe(200);
    });
    }
)
